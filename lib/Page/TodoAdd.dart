import 'package:datetime_picker_formfield_new/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo/Controller.dart';

class TodoAdd extends StatelessWidget {
  final controller = Get.put(Controller());
  @override
  Widget build(BuildContext context) {
    final title = "".obs;
    final dueChanges = false.obs;
    final due = DateTime.now().obs;
    final format = DateFormat("yyyy-MM-dd HH:mma");

    TextEditingController  titleC = TextEditingController();

    titleC.text = title.value;
    return Dialog(child: Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      padding: const EdgeInsets.all(15),
      child: Form(key: controller.todoKey,child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("new".tr, style: Theme.of(context).textTheme.titleLarge),
          const SizedBox(height: 10),
          Text("title".tr),
          TextField(
            keyboardType: TextInputType.name,
            decoration: InputDecoration(
              border: UnderlineInputBorder(borderRadius:BorderRadius.circular(5.0)),
              hintText: "name".tr,
            ),
            onChanged: (value) {
              title(value);
            },
          ),
          const SizedBox(height: 10),
          Text("due".tr),
          DateTimeField(
            decoration: InputDecoration(
              border: UnderlineInputBorder(borderRadius:BorderRadius.circular(5.0)),
              hintText: "due".tr,
            ),
            onChanged: (DateTime? d){
              dueChanges(true);
              due(d);
            },
            format: format,
            onShowPicker: (context, currentValue) async {
              return await showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100),
              ).then((DateTime? date) async {
                if (date != null) {
                  final time = await showTimePicker(
                    context: context,
                    initialTime:
                    TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                  );
                  return DateTimeField.combine(date, time);
                } else {
                  return currentValue;
                }
              });
            },
          ),
          const SizedBox(height: 10),
          Center(child: ElevatedButton(onPressed: (){
            controller.addTodo(title.value, due.value, dueChanges.value);
          }, child: Text("save".tr))),
          Center(child: TextButton(onPressed: (){Get.back();}, child: Text("cancel".tr)))
        ]
      ))
    ));
  }
}
