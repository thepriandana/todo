import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Component/TodoItem.dart';
import '../Controller.dart';

class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(Controller());
    return Scaffold(body: SafeArea(child: SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Container(
          padding: const EdgeInsets.only(top: 25, bottom: 25, left: 20, right:20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("hi".trParams({'name': controller.getName()}), style: Theme.of(context).textTheme.titleLarge),
              const SizedBox(height: 20),
              Obx(() => (controller.getTodo().isEmpty) ?
              Center(child: Text("empty".tr, style: Theme.of(context).textTheme.titleLarge))
                  :
              ListView.builder(itemBuilder: (BuildContext context, int index){
                return TodoItem(index: index);
              }, shrinkWrap: true,
                  itemCount: controller.getTodo().length,
                  physics: const NeverScrollableScrollPhysics()
              ))
            ])
      ),
    )),
      floatingActionButton: FloatingActionButton(onPressed: () { controller.addTodoDialog(); }, child: const Icon(CupertinoIcons.plus),),
    );
  }
}
