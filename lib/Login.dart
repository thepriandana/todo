import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo/Page/Homepage.dart';

import 'Controller.dart';
class Login extends StatefulWidget {
  @override
  State<Login> createState() => _Login();
}

class _Login extends State<Login> {
@override
  Widget build(BuildContext context) {
    final name = "".obs;
    TextEditingController  nameC = TextEditingController();
    nameC.text = name.value;
    final controller = Get.put(Controller(), permanent: true);
    return Scaffold(body: Center(
      child: Container(width: Get.width * 0.8,
        child:
        Form(key: controller.loginKey, child: Column(mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children:[
            Text("name".tr, style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 10),
            TextField(
              keyboardType: TextInputType.name,
              decoration: InputDecoration(
                border: UnderlineInputBorder(borderRadius:BorderRadius.circular(5.0)),
                hintText: "name".tr,
              ),
              onChanged: (value) {
                name(value);
              },
              controller: nameC,
            ),
            const SizedBox(height: 10),
            ElevatedButton(onPressed: () => {controller.login(name.value)}, child: Row(mainAxisSize: MainAxisSize.min, children:[
              Text("next".tr),
              const SizedBox(width: 5),
              const Icon(CupertinoIcons.arrow_right)
            ]))
          ])
        ))
      )
    );
  }
}
