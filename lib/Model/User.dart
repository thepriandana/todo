import 'package:get/get.dart';

import 'Todo.dart';

class User{
  final name = "".obs;
  final todo = <Todo>[].obs;
  User({String? name}){
    this.name(name);
  }
  get getAll => todo.value;
  User.fromJSON(Map<String, dynamic> json){
    try{
      name(json['name'].toString());
      todo(List<Todo>.from(json['todo']?.map((p) => Todo.fromJSON(p))));
    } catch(e){
      print(e);
    }
  }
  Map<String, dynamic> toJson(){
    var map = <String, dynamic>{};
    map["name"] = name.value;
    map["todo"] = todo.value.toList();
    return map;
  }
}