import 'package:get/get.dart';

class Todo{
  final status = 0.obs;
  final title = "".obs;
  final dueDate = DateTime.now().obs;
  Todo({String? title, DateTime? due}){
    status(0); //0 is open & 1 is closed/done
    this.title(title);
    dueDate(due);
  }
  Todo.fromJSON(Map<String, dynamic> json){
    try{
      title(json['title'].toString());
      status(json['status']);
      dueDate(DateTime.parse(json['dueDate']));
    } catch(e){
      print(e);
    }
  }
  Map<String, dynamic> toJson(){
    var map = <String, dynamic>{};
    map["title"] = title.value;
    map["status"] = status.value;
    map["dueDate"] = dueDate.value.toString();
    return map;
  }

}