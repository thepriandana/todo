import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:todo/Page/Homepage.dart';
import 'package:todo/Page/TodoAdd.dart';

import 'Model/Todo.dart';
import 'Model/User.dart';

class Controller extends GetxController{
  final currentID = 0.obs;
  final users = <User>[].obs;
  late GlobalKey<FormState> loginKey;
  late GlobalKey<FormState> todoKey;
  Controller(){
    loginKey = GlobalKey<FormState>();
    todoKey = GlobalKey<FormState>();
    refreshX();
  }
  refreshX() async{
    //open todo at users
    if(Hive.box("todo").get("users") != null){
      var objJson = jsonDecode(Hive.box("todo").get("users")) as List;
      List<User> preList = objJson.map((a) => User.fromJSON(a)).toList();
      users(preList);
    }
  }
  String getName(){
    return users.value.elementAt(currentID.value).name.value;
  }
  void saveBox() async {
    await Hive.box("todo").put("users", jsonEncode(users.map((i) => i.toJson()).toList()).toString());
  }
  void login(String name) {
    //validate first
    if(name.isEmpty){
      Get.snackbar("error".tr, "error_login".tr);
    }else{
      //if none found then create one;
      int i=0;
      bool found = false;
      for(User u in users){
        if(u.name.value == name){
          currentID(i);
          found = true;
          break;
        }
        i++;
      }
      if(!found){
        //add new one
        users.add(User(name: name));
        currentID(users.length-1);
        //save on hive
        saveBox();
      }
      Get.to(() => Homepage());
    }
  }
  List<Todo> getTodo(){
    return users.elementAt(currentID.value).getAll;
  }
  void addTodoDialog(){
    Get.dialog(TodoAdd());
  }
  void addTodo(String title, DateTime due, bool dueChanges) async {
    if(title.isEmpty){
      Get.snackbar("error".tr, "error_title".tr);
    }else if(!dueChanges){
      Get.snackbar("error".tr, "error_date".tr);
    }else{
      users.elementAt(currentID.value).todo.add(Todo(title: title, due: due));
      saveBox();
      Get.back();
    }
  }
  void removeTodo(int index){
    users.elementAt(currentID.value).todo.removeAt(index);
    saveBox();
  }
  void doneTodo(int index){
    users.elementAt(currentID.value).todo.elementAt(index).status(1);
    saveBox();
  }
}