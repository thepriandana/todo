import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Controller.dart';
import '../Model/Todo.dart';
import 'package:intl/intl.dart';
import 'DueSpan.dart';

class TodoItem extends StatelessWidget {
  late Todo todo;
  int index;
  TodoItem({required this.index});
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(Controller());
    todo = controller.getTodo().elementAt(index);
    String due = DateFormat('yyyy-MM-dd – kk:mma').format(todo.dueDate.value);
    return Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).backgroundColor),
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.only(top: 15, bottom: 15, left: 20, right: 20),
      child: Obx( () => Column(mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children:[
          Row(children:[
            DueSpan(date: todo.dueDate.value, done: todo.status.value == 1),
            const Expanded(child: SizedBox()),
            InkWell(onTap: (){
              controller.removeTodo(index);
            }, child: Container(
              decoration: BoxDecoration(color: Theme.of(context).primaryColor, borderRadius: BorderRadius.circular(5)),
                padding: const EdgeInsets.all(5), child: const Icon(CupertinoIcons.trash))
            ),
          ]),
          const SizedBox(height: 5),
          Text("${todo.title}", style: Theme.of(context).textTheme.titleLarge),
          Row(children:[
            Expanded(child:
              Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                Text("due".tr),
                Text("$due")
              ])
            ),
            ((todo.status.value == 0) ? ElevatedButton(onPressed: (){
              controller.doneTodo(index);
            }, child: Text("done".tr)) : SizedBox())
          ])
        ]
      ))
    );
  }
}
