import 'package:flutter/material.dart';

class DueSpan extends StatelessWidget {
  late DateTime date;
  late bool done;
  DueSpan({required this.date, required this.done});
  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    TextStyle? textStyle = Theme.of(context).textTheme.titleSmall;
    if(done == false && now.compareTo(date) < 0) {
      textStyle = textStyle?.merge(const TextStyle(color: Colors.black));
    } else {
      textStyle = textStyle?.merge(const TextStyle(color: Colors.white));
    }
    return Container(
      padding: const EdgeInsets.only(top: 5, bottom: 5, left: 8, right: 8),
        decoration: BoxDecoration(color: (done) ? Colors.green : (now.compareTo(date) < 0) ? Colors.grey.shade200 : Colors.red,
          borderRadius: BorderRadius.circular(100)
        ),
        child: (done) ? Text("Done", style: textStyle) :
      (now.compareTo(date) < 0) ? Text("Pending", style: textStyle) : Text("Overdue", style: textStyle)
    );
  }
}
