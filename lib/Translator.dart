import 'package:get/get.dart';

class Translator extends Translations{
  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys => {
    'en_US':{
      'hi': 'Hi, @name',
      'name': 'Name',
      'next': 'Next',
      'new': 'New Todo',
      'title': 'Title',
      'due': 'Due date',
      'save': 'Save',
      'cancel': 'Cancel',
      'done': 'Done',
      'empty': 'Todo still empty',
      'error': 'Error',
      'error_login' : 'Please insert a name',
      'error_title' : 'Please fill title',
      'error_date': 'Please insert due date'
    }
  };

}