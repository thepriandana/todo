import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

import 'Login.dart';
import 'Translator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;
  Hive.init(appDocPath);
  await Hive.openBox('todo');
  runApp(
      GetMaterialApp(home: Login(),
        translations: Translator(),
        locale: Locale('en', 'US'),
        fallbackLocale: Locale('en', 'US'),
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.lightBlue[800],
        ),
      )
  );
}